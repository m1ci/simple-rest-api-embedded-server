package db;
import java.util.Hashtable;
 
public class DB {
    private static DB instance = null;
    private Hashtable<Integer, Country> ht = new Hashtable();
 
    public static DB getInstance() {
        if (instance == null)
            instance = new DB();
        return instance;
    }
    public void addObject(Country t){
        ht.put(t.getId(), t);
    }
    public Country getObject(int id) {
        return ht.get(id);
    }
}